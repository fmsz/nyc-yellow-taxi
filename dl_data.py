import os
import urllib.request
import datetime as dt
from dateutil import rrule

DATA_DIR = 'data'
START_DATE = '2020-01'
END_DATE = '2021-07'  # data is available only until July
DATE_FORMAT = '%Y-%m'
ROOT = 'https://s3.amazonaws.com/nyc-tlc/trip+data/yellow_tripdata_'
EXTENSION = 'csv'


def create_data_dir():
    if not os.path.isdir(DATA_DIR):
        os.makedirs(DATA_DIR)
    os.chdir(DATA_DIR)


def generate_dates():
    start_date, end_date = dt.datetime.strptime(START_DATE, DATE_FORMAT), dt.datetime.strptime(END_DATE, DATE_FORMAT)
    return list(rrule.rrule(rrule.MONTHLY, dtstart=start_date, until=end_date))


def download_data(dates):
    for date in dates:
        dl_url = ROOT + dt.date.strftime(date, DATE_FORMAT) + '.' + EXTENSION
        filename = dl_url.rsplit('/', 1)[-1]

        if not os.path.isfile(filename):
            print(f'Downloading {dl_url} START')
            urllib.request.urlretrieve(dl_url, filename)  # let's hope the files won't be too big
            print(f'Downloading {dl_url} END')
        else:
            print(f'{filename} already exists moving on')


def main():
    create_data_dir()
    dates = generate_dates()
    download_data(dates)


if __name__ == '__main__':
    main()
